# shorty-android
Android app to pick a random short film coming from https://www.shortoftheweek.com/.

## About
Want to watch some movie or series, but don't know what exactly?  Scrolling endlessly without getting to a decision only to finally turn down your device?

Just pick a random short film instead! It won't take long watching it (promise :wink:) and you might discover one or the other gem! If you feel like you're up to another round, just hit the surprise button and you are good to go.

Fortunately, [shortoftheweek](https://www.shortoftheweek.com/) offers a great platform curating shorts and giving filmmakers their audience. If you feel like scrolling endlessly again, why not checkout out [shortoftheweek](https://www.shortoftheweek.com/) directly?

Unfortunately, there is no random choice already. So I decided to build this app.

## Credits
Credits go out to [shortoftheweek](https://www.shortoftheweek.com/) and all the filmmakers providing their content.

This project was build by me for fun. Except that it exploits the existing API from [shortoftheweek](https://www.shortoftheweek.com/), it is totally unrelated.

## License
This project is licensed under [MIT](LICENSE)
