package dev.bauerei.shorty

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShortyApplication : Application()