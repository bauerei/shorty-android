package dev.bauerei.shorty

import dev.bauerei.shorty.domain.ShortFilmDTO
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*

interface ShortyClient {
    suspend fun getRandomShortFilm(): ShortFilmDTO
}

class ShortyClientImpl : ShortyClient {
    private val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = GsonSerializer {
                setPrettyPrinting()
                disableHtmlEscaping()
            }
        }
    }

    override suspend fun getRandomShortFilm(): ShortFilmDTO =
        httpClient.get("https://shorty-be.azurewebsites.net/api/random")
}