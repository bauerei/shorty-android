package dev.bauerei.shorty.tv.presentation

object Routes {
    const val Browser = "browser"
    const val Film = "film"
}