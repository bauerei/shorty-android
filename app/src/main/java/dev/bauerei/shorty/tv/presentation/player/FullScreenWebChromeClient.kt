package dev.bauerei.shorty.tv.presentation.player

import android.app.Activity
import android.util.Log
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import dev.bauerei.shorty.ui.theme.ShortyTheme

class FullScreenWebChromeClient(
    private val activity: Activity,
    private val playerControls: @Composable () -> Unit,
) : WebChromeClient() {
    private var fullScreenView: View? = null

    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
        Log.i(TAG, "onShowCustomView")
        super.onShowCustomView(view, callback)

        if (fullScreenView != null) {
            onHideCustomView()
            return
        }

        // create new View
        fullScreenView = FrameLayout(activity.applicationContext).apply {
            // containing the full screen player
            addView(
                view,
                FrameLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT
                )
            )
            // drawing the player controls on top
            addView(
                ComposeView(activity.applicationContext).apply {
                    setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
                    setContent {
                        ShortyTheme {
                            playerControls()
                        }
                    }
                }
            )
        }

        (activity.window.decorView as FrameLayout).addView(
            fullScreenView,
            FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            )
        )
    }

    override fun onHideCustomView() {
        Log.i(TAG, "onHideCustomView")
        super.onHideCustomView()

        (activity.window.decorView as FrameLayout).removeView(fullScreenView)
        fullScreenView = null
    }

    companion object {
        private val TAG = FullScreenWebChromeClient::class.simpleName
    }
}