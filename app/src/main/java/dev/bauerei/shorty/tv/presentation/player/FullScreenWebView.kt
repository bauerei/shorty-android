package dev.bauerei.shorty.tv.presentation.player

import android.app.Activity
import android.view.ViewGroup.LayoutParams
import android.webkit.WebView
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.viewinterop.AndroidView

@Composable
fun FullScreenWebView(
    url: String,
    onWebViewCreated: (PlayerControlsHandle) -> Unit,
    playerControls: @Composable (PlayerControlsHandle) -> Unit,
    modifier: Modifier = Modifier
) {
    val activity = LocalView.current.context as Activity

    AndroidView(
        modifier = modifier,
        factory = {
            WebView(it).apply {
                layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                settings.javaScriptEnabled = true
                settings.mediaPlaybackRequiresUserGesture = false
                webChromeClient = FullScreenWebChromeClient(
                    activity = activity,
                    playerControls = { playerControls(WebViewPlayerControlsHandle(this)) },
                )
            }.also { webView ->
                onWebViewCreated(WebViewPlayerControlsHandle(webView))
            }
        },
        update = { webView ->
            val html =
                """
                 <iframe id="iframe" src="$url?controls=false" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                 <script src="https://player.vimeo.com/api/player.js"></script>
                 <script>
                     var iframe = document.getElementById('iframe');
                     var player = new Vimeo.Player(iframe);
                     
                     function ${FullScreenWebView.EXIT_FULLSCREEN}() {
                         player.exitFullscreen();
                     };
                     
                     function ${FullScreenWebView.PLAY_FULLSCREEN}() {
                         console.log('Requesting fullscreen');
                         player.requestFullscreen().then(function() {
                             console.log('Entered fullscreen');
                             player.play();
                         }).catch(function(error) {
                             console.log('Error on fullscreen:' + error);
                         });
                     };
                     
                     function ${FullScreenWebView.PAUSE}() {
                         player.pause();
                     };
                     
                     function ${FullScreenWebView.PLAY}() {
                         player.play();
                     };
                 </script>
                 """.trimIndent()
            webView.loadHtml(html)
        }
    )
}

private object FullScreenWebView {
    const val EXIT_FULLSCREEN = "exitFullscreen"
    const val PLAY_FULLSCREEN = "playFullscreen"
    const val PAUSE = "pause"
    const val PLAY = "play"
}

fun WebView.playFullScreen() {
    this.executeFunction(FullScreenWebView.PLAY_FULLSCREEN)
}

fun WebView.play() {
    this.executeFunction(FullScreenWebView.PLAY)
}

fun WebView.pause() {
    this.executeFunction(FullScreenWebView.PAUSE)
}

fun WebView.exitFullScreen() {
    this.executeFunction(FullScreenWebView.EXIT_FULLSCREEN)
}

private fun WebView.executeFunction(function: String) {
    this.loadUrl("javascript:(function() { $function(); })()")
}

private fun WebView.loadHtml(html: String) {
    this.loadData(html, "text/html; charset=utf-8", "UTF-8")
}