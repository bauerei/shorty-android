package dev.bauerei.shorty.tv.presentation.player

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.focus.focusRequester
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dev.bauerei.shorty.domain.ShortFilmDTO

@Composable
fun Player(
    shortFilm: ShortFilmDTO,
    onBackPressed: () -> Unit,
    modifier: Modifier = Modifier,
    viewModel: PlayerViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    var playerControlsHandle: PlayerControlsHandle? by remember { mutableStateOf(null) }
    val focusRequester = remember { FocusRequester() }

    PlayerOverlay(
        previewImage = "https:${shortFilm.thumbnail}",
        canPlay = playerControlsHandle != null,
        onPlayClicked = { playerControlsHandle?.playFullScreen() },
        modifier = modifier
    ) {
        FullScreenWebView(
            url = shortFilm.film_url,
            onWebViewCreated = {
                Log.i(Player.TAG, "onWebViewCreated")
                playerControlsHandle = it
            },
            playerControls = { playerHandle ->
                updatePlayer(playerHandle, uiState, viewModel)

                PlayerControls(
                    playbackState = uiState.playbackState,
                    visibility = uiState.playerControlsVisibility,
                    onPlayClick = viewModel::togglePlay,
                    onClick = viewModel::showPlayerControls,
                    modifier = Modifier
                        .fillMaxSize()
                        .focusRequester(focusRequester)
                )

                if (uiState.playerControlsVisibility == PlayerControlsVisibility.HIDDEN) {
                    LaunchedEffect(Unit) {
                        focusRequester.requestFocus()
                    }
                }
            },
            modifier = Modifier.focusProperties { canFocus = false }
        )
    }

    BackHandler {
        Log.i(Player.TAG, "BackHandler")
        playerControlsHandle?.exitFullScreen()
        onBackPressed()
    }
}

private fun updatePlayer(
    playerControlsHandle: PlayerControlsHandle,
    playerState: PlayerState,
    viewModel: PlayerViewModel
) =
    when (playerState.playbackState) {
        PlaybackState.ON_PLAY -> {
            playerControlsHandle.play()
            viewModel.onPlay()
        }

        PlaybackState.ON_PAUSE -> {
            playerControlsHandle.pause()
            viewModel.onPause()
        }

        else -> {} // nothing to do
    }

private object Player {
    val TAG = Player::class.simpleName
}