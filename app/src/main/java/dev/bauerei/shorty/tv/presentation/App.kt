package dev.bauerei.shorty.tv.presentation

import android.util.Log
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dev.bauerei.shorty.ShortyClient
import dev.bauerei.shorty.domain.ShortFilmDTO
import dev.bauerei.shorty.tv.LoadedShortFilms
import dev.bauerei.shorty.tv.LoadingShortFilms
import dev.bauerei.shorty.tv.State
import dev.bauerei.shorty.tv.presentation.browser.ShortFilmBrowser
import dev.bauerei.shorty.tv.presentation.player.Player
import io.ktor.client.features.ServerResponseException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun App(client: ShortyClient) {
    var appState: State by remember { mutableStateOf(LoadingShortFilms) }
    var shortFilms by rememberSaveable { mutableStateOf(listOf<ShortFilmDTO>()) }
    var selectedFilm by rememberSaveable { mutableIntStateOf(0) }
    val navController = rememberNavController()

    fun onFilmClicked(shortFilmDTO: ShortFilmDTO) {
        navController.navigate(Routes.Film)
    }

    fun onFilmSelected(newSelectedFilmIndex: Int) {
        selectedFilm = newSelectedFilmIndex
        // when end of film list is reached, trigger loading films
        if (newSelectedFilmIndex == shortFilms.lastIndex) {
            appState = LoadingShortFilms
        }
    }

    NavHost(navController = navController, startDestination = Routes.Browser) {
        composable(Routes.Browser) {
            if (shortFilms.isNotEmpty()) {
                ShortFilmBrowser(
                    shortFilms = shortFilms,
                    selectedFilmIndex = selectedFilm,
                    appState = appState,
                    onFilmClick = ::onFilmClicked,
                    onFilmSelect = ::onFilmSelected,
                    modifier = Modifier.fillMaxSize()
                )
            }
            when (appState) {
                is LoadingShortFilms -> {
                    LaunchedEffect(Unit) {
                        launch(Dispatchers.IO) {
                            val newFilms =
                                (0 until App.BatchSizeNumberOfFilms).mapNotNull { index ->
                                    try {
                                        client.getRandomShortFilm()
                                    } catch (e: ServerResponseException) {
                                        Log.w(App.Tag, "Ignoring 500", e)
                                        null
                                    }
                                }
                            shortFilms = shortFilms + newFilms
                            appState = LoadedShortFilms
                        }

                    }
                }

                is LoadedShortFilms -> {}
            }
        }
        composable(Routes.Film) {
            Player(
                shortFilm = shortFilms[selectedFilm],
                onBackPressed = { navController.popBackStack() },
                modifier = Modifier.fillMaxSize(),
            )
        }
    }
}

object App {
    val Tag = App::class.qualifiedName
    const val BatchSizeNumberOfFilms = 5
}