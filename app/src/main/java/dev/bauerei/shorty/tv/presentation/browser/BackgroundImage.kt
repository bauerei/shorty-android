package dev.bauerei.shorty.tv.presentation.browser

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.tv.material3.Text
import coil.compose.AsyncImage
import dev.bauerei.shorty.domain.ShortFilmDTO
import dev.bauerei.shorty.ui.theme.md_theme_dark_onBackground

@Composable
fun BackgroundImage(shortFilm: ShortFilmDTO, modifier: Modifier = Modifier) {
    BoxWithConstraints(
        modifier = modifier
            .background(Color.Black)
            .fillMaxSize()
    ) {
        AsyncImage(
            modifier = Modifier
                .height(maxHeight * 2 / 3)
                .padding(start = BackgroundImage.PaddingLeft)
                .fadingEdge(),
            model = "https:${shortFilm.thumbnail}",
            contentDescription = shortFilm.title,
            contentScale = ContentScale.Crop
        )
        Column(
            modifier = Modifier
                .padding(start = ShortFilmBrowser.PaddingListLeft)
                .align(Alignment.BottomStart)
                .offset(y = -ShortFilmBrowser.PaddingListLeft - ShortFilmBrowser.CardHeight - BackgroundImage.YOffset)
                .width(maxWidth / 2)
                .fillMaxWidth()
        ) {
            Text(
                color = md_theme_dark_onBackground,
                style = MaterialTheme.typography.headlineSmall,
                text = shortFilm.title.orEmpty()
            )
            Text(
                color = md_theme_dark_onBackground,
                maxLines = 3,
                overflow = TextOverflow.Clip,
                style = MaterialTheme.typography.bodySmall,
                text = shortFilm.short_description
            )
        }
    }
}

object BackgroundImage {
    val PaddingLeft = 202.dp
    val YOffset = 50.dp
}