package dev.bauerei.shorty.tv.presentation.browser

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.tv.material3.Card
import coil.compose.AsyncImage
import dev.bauerei.shorty.domain.ShortFilmDTO

@Composable
fun ShortFilmCard(
    shortFilm: ShortFilmDTO,
    onFilmClick: (ShortFilmDTO) -> Unit,
    onFilmSelect: (ShortFilmDTO) -> Unit,
    modifier: Modifier = Modifier
) {
    Card(
        onClick = { onFilmClick(shortFilm) },
        modifier = modifier
            .width(200.dp)
            .aspectRatio(16f / 9)
            .onFocusChanged {
                if (it.hasFocus) {
                    onFilmSelect(shortFilm)
                }
            }
    ) {
        AsyncImage(
            model = "https:${shortFilm.thumbnail}",
            contentDescription = shortFilm.title,
            contentScale = ContentScale.Crop,
        )
    }
}