package dev.bauerei.shorty.tv.presentation.browser

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.bauerei.shorty.domain.ShortFilmDTO
import dev.bauerei.shorty.tv.LoadedShortFilms
import dev.bauerei.shorty.tv.LoadingShortFilms
import dev.bauerei.shorty.tv.State
import dev.bauerei.shorty.ui.theme.ShortyTheme

@Composable
fun ShortFilmBrowser(
    shortFilms: List<ShortFilmDTO>,
    selectedFilmIndex: Int,
    appState: State,
    onFilmClick: (ShortFilmDTO) -> Unit,
    onFilmSelect: (Int) -> Unit,
    modifier: Modifier = Modifier,
) {
    val focusRequester = remember { FocusRequester() }
    val selectedFilm = shortFilms[selectedFilmIndex]

    LaunchedEffect(selectedFilmIndex) {
        focusRequester.requestFocus()
    }

    // Container
    Box(modifier = modifier) {
        // Background
        BackgroundImage(
            shortFilm = selectedFilm,
        )

        // Rows
        LazyRow(
            modifier = Modifier.align(Alignment.BottomEnd),
            horizontalArrangement = Arrangement.spacedBy(20.dp),
            contentPadding = PaddingValues(start = 58.dp, bottom = 86.dp, end = 58.dp),
        ) {
            itemsIndexed(items = shortFilms) { index, shortFilm ->
                ShortFilmCard(
                    shortFilm = shortFilm,
                    onFilmClick = onFilmClick,
                    onFilmSelect = { onFilmSelect(index) },
                    modifier = Modifier
                        .then(if (index == selectedFilmIndex) Modifier.focusRequester(focusRequester) else Modifier)
                )
            }
            if (appState == LoadingShortFilms) {
                item {
                    // puts progress indicator within same dimensions as a Card
                    Column(
                        modifier = Modifier
                            .width(200.dp)
                            .aspectRatio(16f / 9),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
    }
}

@Preview(uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
private fun ShortFilmBrowserPreview() {
    ShortyTheme {
        ShortFilmBrowser(
            shortFilms = listOf(
                ShortFilmDTO(
                    film_url = "",
                    short_description = "Description",
                    thumbnail = null,
                    title = "Title",
                    progressive_play_links = emptyList()
                )
            ),
            selectedFilmIndex = 0,
            appState = LoadedShortFilms,
            onFilmClick = {},
            onFilmSelect = {}
        )
    }
}

object ShortFilmBrowser {
    val CardHeight = 150.dp
    val PaddingListLeft = 58.dp
}