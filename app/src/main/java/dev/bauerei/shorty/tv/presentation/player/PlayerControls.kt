package dev.bauerei.shorty.tv.presentation.player

import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.key.type
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.bauerei.shorty.R
import dev.bauerei.shorty.ui.theme.ShortyTheme

@Composable
fun PlayerControls(
    playbackState: PlaybackState,
    visibility: PlayerControlsVisibility,
    onPlayClick: () -> Unit,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier = modifier
        .focusable()
        .onKeyEvent { keyEvent ->
            if (keyEvent.type == KeyEventType.KeyDown) {
                if (visibility == PlayerControlsVisibility.HIDDEN && keyEvent.key == Key.DirectionCenter) {
                    onPlayClick()
                } else if (visibility == PlayerControlsVisibility.HIDDEN) {
                    onClick()
                }
            }

            false
        }
    ) {
        if (visibility == PlayerControlsVisibility.VISIBLE) {
            Row(
                modifier = modifier,
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                PlayButton(playbackState, onPlayClick)
            }
        }
    }
}

@Composable
private fun PlayButton(
    playbackState: PlaybackState,
    onPlayClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    val buttonSize = 56.dp
    IconButton(
        onClick = onPlayClicked,
        colors = IconButtonDefaults.iconButtonColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
            contentColor = MaterialTheme.colorScheme.onPrimaryContainer
        ),
        modifier = modifier.size(buttonSize)
    ) {
        if (playbackState == PlaybackState.PLAYING) {
            Icon(
                painter = painterResource(R.drawable.baseline_pause_24),
                contentDescription = "Pause",
                modifier = Modifier.size(buttonSize / 2)
            )
        } else {
            Icon(
                imageVector = Icons.Filled.PlayArrow,
                contentDescription = "Play",
                modifier = Modifier.size(buttonSize / 2)
            )
        }
    }
}

@Preview
@Composable
private fun PlayerControlsPlayingPreview() {
    ShortyTheme {
        PlayerControls(
            playbackState = PlaybackState.PLAYING,
            visibility = PlayerControlsVisibility.VISIBLE,
            onPlayClick = {},
            onClick = {},
        )
    }
}

@Preview
@Composable
private fun PlayerControlsStoppedPreview() {
    ShortyTheme {
        PlayerControls(
            playbackState = PlaybackState.PAUSED,
            visibility = PlayerControlsVisibility.VISIBLE,
            onPlayClick = {},
            onClick = {},
        )
    }
}