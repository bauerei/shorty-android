package dev.bauerei.shorty.tv.presentation.player

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import dev.bauerei.shorty.ui.theme.ShortyTheme

@Composable
fun PlayerOverlay(
    previewImage: String,
    canPlay: Boolean,
    onPlayClicked: () -> Unit,
    modifier: Modifier = Modifier,
    player: @Composable () -> Unit,
) {
    val focusRequester = remember { FocusRequester() }
    Box(modifier = modifier) {
        player()

        PreviewImage(previewImage = previewImage, modifier = Modifier.fillMaxSize())

        if (canPlay) {
            PlayerControls(
                playbackState = PlaybackState.PAUSED,
                visibility = PlayerControlsVisibility.VISIBLE,
                onPlayClick = onPlayClicked,
                onClick = {},
                modifier = Modifier
                    .align(Alignment.Center)
                    .focusRequester(focusRequester)
            )
            LaunchedEffect(Unit) {
                focusRequester.requestFocus()
            }
        }
    }
}

@Composable
private fun PreviewImage(previewImage: String, modifier: Modifier = Modifier) {
    AsyncImage(
        modifier = modifier,
        model = previewImage,
        contentDescription = null,
        contentScale = ContentScale.Crop
    )
}

@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun PlayerOverlayPreview() {
    ShortyTheme {
        PlayerOverlay(
            previewImage = "",
            canPlay = true,
            onPlayClicked = {}
        ) {}
    }
}