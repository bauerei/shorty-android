package dev.bauerei.shorty.tv

sealed interface State

data object LoadingShortFilms : State

data object LoadedShortFilms : State