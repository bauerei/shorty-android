package dev.bauerei.shorty.tv.presentation.player

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlayerViewModel @Inject constructor() : ViewModel() {
    private val _uiState = MutableStateFlow(PlayerState())
    val uiState: StateFlow<PlayerState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            hidePlayerControlsWithDelay()
        }
    }

    fun onPause() = _uiState.update { it.copy(playbackState = PlaybackState.PAUSED) }

    fun onPlay() = _uiState.update { it.copy(playbackState = PlaybackState.PLAYING) }

    fun togglePlay() {
        val newPlaybackState = when (_uiState.value.playbackState) {
            PlaybackState.PLAYING, PlaybackState.ON_PLAY -> PlaybackState.ON_PAUSE
            PlaybackState.PAUSED, PlaybackState.ON_PAUSE -> PlaybackState.ON_PLAY
        }

        _uiState.update {
            it.copy(
                playbackState = newPlaybackState,
                playerControlsVisibility = PlayerControlsVisibility.VISIBLE
            )
        }

        hidePlayerControlsWithDelay()
    }

    fun showPlayerControls() {
        _uiState.update { it.copy(playerControlsVisibility = PlayerControlsVisibility.VISIBLE) }

        hidePlayerControlsWithDelay()
    }

    private fun hidePlayerControlsWithDelay() {
        viewModelScope.launch {
            if (_uiState.value.playbackState in setOf(
                    PlaybackState.PLAYING,
                    PlaybackState.ON_PLAY
                )
            ) {
                delay(CONTROLS_VISIBILITY_DELAY)
                _uiState.update { it.copy(playerControlsVisibility = PlayerControlsVisibility.HIDDEN) }
            }
        }
    }

    companion object {
        const val CONTROLS_VISIBILITY_DELAY = 3000L
    }
}

enum class PlayerControlsVisibility {
    HIDDEN,
    VISIBLE,
}

enum class PlaybackState {
    ON_PLAY,
    PLAYING,
    ON_PAUSE,
    PAUSED,
}

data class PlayerState(
    val playbackState: PlaybackState = PlaybackState.PLAYING,
    val playerControlsVisibility: PlayerControlsVisibility = PlayerControlsVisibility.VISIBLE,
)