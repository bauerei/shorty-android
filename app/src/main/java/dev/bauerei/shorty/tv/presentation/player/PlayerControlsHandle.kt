package dev.bauerei.shorty.tv.presentation.player

import android.webkit.WebView

interface PlayerControlsHandle {
    fun play()
    fun pause()
    fun playFullScreen()
    fun exitFullScreen()
}

class WebViewPlayerControlsHandle(private val webView: WebView) : PlayerControlsHandle {
    override fun play() {
        webView.play()
    }

    override fun pause() {
        webView.pause()
    }

    override fun playFullScreen() {
        webView.playFullScreen()
    }

    override fun exitFullScreen() {
        webView.exitFullScreen()
    }

}