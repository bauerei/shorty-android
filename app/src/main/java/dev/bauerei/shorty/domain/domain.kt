package dev.bauerei.shorty.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlayLinkDTO(val quality: String, val url: String) : Parcelable

@Parcelize
data class ShortFilmDTO(
    val film_url: String,
    val short_description: String,
    val thumbnail: String?,
    val title: String?,
    val progressive_play_links: List<PlayLinkDTO>
) : Parcelable // naming with underscore for Gson parsing
