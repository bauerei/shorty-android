package dev.bauerei.shorty

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.graphics.res.animatedVectorResource
import androidx.compose.animation.graphics.res.rememberAnimatedVectorPainter
import androidx.compose.animation.graphics.vector.AnimatedImageVector
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.StyledPlayerView
import dev.bauerei.shorty.domain.ShortFilmDTO
import dev.bauerei.shorty.ui.theme.ShortyTheme

class MainActivity : ComponentActivity() {
    private val client = ShortyClientImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ShortyTheme {
                Surface {
                    Shorty()
                }
            }
        }
    }

    @Composable
    fun Shorty() {
        var appState: State by remember { mutableStateOf(LoadingShortFilm) }

        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Row(modifier = Modifier.weight(5f)) {
                when (appState) {
                    is LoadingShortFilm -> {
                        LaunchedEffect(Unit) {
                            val shortFilm: ShortFilmDTO =
                                client.getRandomShortFilm()

                            appState = LoadedShortFilm(shortFilm)
                        }

                        LoadingScreen()
                    }
                    is LoadedShortFilm -> {
                        ShortFilm(shortFilm = (appState as LoadedShortFilm).shortFilm)
                    }
                }
            }
            Row(
                modifier = Modifier
                    .weight(1f)
                    .padding(15.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.Bottom,
                horizontalArrangement = Arrangement.Center,
            ) {
                ShuffleButton { appState = LoadingShortFilm }
            }
        }
    }

    @OptIn(ExperimentalAnimationGraphicsApi::class)
    @Composable
    fun LoadingScreen() {
        var atEnd by remember { mutableStateOf(false) }

        LaunchedEffect(Unit) { // required to trigger animation
            atEnd = !atEnd
        }

        Column {
            Row(
                modifier = Modifier
                    .weight(2f)
                    .padding(15.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAnimatedVectorPainter(
                        animatedImageVector = AnimatedImageVector.animatedVectorResource(R.drawable.avd_ic_spinner),
                        atEnd = atEnd
                    ),
                    "",
                    modifier = Modifier.size(128.dp),
                    alignment = Alignment.Center,
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
                )
            }
            Row(modifier = Modifier.weight(3f)) {}
        }
    }

    @Composable
    fun ShortFilm(shortFilm: ShortFilmDTO) {
        val maxDefaultQuality = "720p"
        val playLink = shortFilm.progressive_play_links
            .sortedByDescending { it.quality }
            .find { it.quality <= maxDefaultQuality }

        Column {
            Row(modifier = Modifier.weight(2f)) {
                if (playLink == null) {
                    Text(text = "Could not get play link for any quality equals or below max default quality of $maxDefaultQuality")
                } else {
                    Log.i(null, "Picked play link with quality ${playLink.quality}")
                    VideoPlayer(playLink.url)
                }
            }
            Row(
                modifier = Modifier
                    .weight(3f)
                    .padding(15.dp)
            ) {
                Text(text = shortFilm.short_description)
            }
        }
    }

    @Composable
    fun ShuffleButton(onClick: () -> Unit) {
        FloatingActionButton(
            onClick = onClick,
            containerColor = MaterialTheme.colorScheme.primary
        ) {
            Icon(Icons.Filled.Refresh, "", tint = MaterialTheme.colorScheme.onPrimary)
        }
    }

    @Composable
    fun VideoPlayer(url: String) {
        val context = LocalContext.current

        // Create player
        val exoPlayer = remember {
            ExoPlayer.Builder(context).build().apply {
                setMediaItem(MediaItem.fromUri(url))
                playWhenReady = false
                prepare()
            }
        }

        // Player view
        AndroidView(
            factory = {
                // Bind player to player view
                StyledPlayerView(context).apply {
                    player = exoPlayer
                    layoutParams = FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                }
            }
        )
    }
}

sealed interface State

object LoadingShortFilm : State

data class LoadedShortFilm(val shortFilm: ShortFilmDTO) : State